
A simple script to make Publishing Miscreated mods a little faster...

This script will help to:

* Create both Editor and Workshop .PAK file for you from files in the __source_ directory
* Create a _mod.vdf_ based on user input when needed
* Publish/Update Mod to the workshop Via SteamCMD

### How to Use

* Download using the link to the left
* Extract the `MisModKitchen` folder to wherever you want to keep MisModKitchen and your Mods
* Go into the `WORKSPACE` folder and each time you create a new mod Duplicate the `_Template` folder then rename it to whatever {suggest mod name for simplicity}...
* Add you Mods files to the source folder eg: `_Source\GameSDK\Scripts\mods\MyMod.lua`


* run `CreateWorkshopPakFromSource.bat` or `CreateEditorPakFromSource.bat` respectively to create your PAK files (will be placed in correct folders automatically)


* run `UploadToWorkshop.bat` and enter the required information to create your _mod.vdf_ then publish via SteamCMD

when updating a mod as long as the _mod.vdf_ file exists in the mods folder `UploadToWorkshop.bat` will update the existing workshop entry instead...

This Pack includes a copy of `SteamCMD` for workshop uploads and a copy of `7zip` for creation of Pak files they are the original files unmodified.  
(just replace them if you're not sure)

#### Extra

I have included a basic VS-Code Workspace configuration similar to the one I use with MisModKitchen myself.  
To make the best use of it I recommend the following VS-Code plugins:

> Name: Task Explorer   
> Id: spmeesseman.vscode-taskexplorer   
> Description: Manage tasks for npm, vscode, ant, gradle, grunt, gulp, batch, bash, make, python, perl, powershell, ruby, and nsis
>
> VS Marketplace Link: [https://marketplace.visualstudio.com/items?itemName=spmeesseman.vscode-taskexplorer](https://marketplace.visualstudio.com/items?itemName=spmeesseman.vscode-taskexplorer "https://marketplace.visualstudio.com/items?itemName=spmeesseman.vscode-taskexplorer")

> Name: Todo Tree  
> Id: gruntfuggly.todo-tree   
> Description: Show TODO, FIXME, etc. comment tags in a tree view
>
> VS Marketplace Link: [https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree](https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree "https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree")

> Name: Lua Id: sumneko.lua   
> Description: Lua Language Server coded by Lua
>
> VS Marketplace Link: [https://marketplace.visualstudio.com/items?itemName=sumneko.lua](https://marketplace.visualstudio.com/items?itemName=sumneko.lua "https://marketplace.visualstudio.com/items?itemName=sumneko.lua")

> Name: Lua Analyzer Id: stuartwang.lua-analyzer   
> Description: lua code analysis tool
>
> VS Marketplace Link: [https://marketplace.visualstudio.com/items?itemName=stuartwang.lua-analyzer](https://marketplace.visualstudio.com/items?itemName=stuartwang.lua-analyzer "https://marketplace.visualstudio.com/items?itemName=stuartwang.lua-analyzer")

> Name: Better Comments Id: aaron-bond.better-comments   
> Description: Improve your code commenting by annotating with an alert, informational, TODOs, and more!
>
> VS Marketplace Link: [https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments](https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments "https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments")

with the above Plugins Enabled, You'll have something like this (Using One Dark+ Theme)

![](https://gitlab.com/svaltek/public/mismodkitchen/raw/master/Screen_VSCODE_Workspace.png)