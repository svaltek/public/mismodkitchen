@echo off
set thisPATH=%~dp0
set SourceDir=%thisPATH%_Source
set editorBuildDir=%thisPATH%EDITOR_BUILD
set /p fName="pakfile name: "
echo Files in source:
DIR /S /B %SourceDir%
..\..\_7z\7za.exe a -tzip -mx0 -xr!*.md -xr!.git* "%editorBuildDir%\%fName%.pak" "%SourceDir%\GameSDK\*"
pause