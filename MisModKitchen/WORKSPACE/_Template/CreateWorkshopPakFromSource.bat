@echo off
set thisPATH=%~dp0
set SourceDir=%thisPATH%_Source
set workshopBuildDir=%thisPATH%WORKSHOP_BUILD
set /p fName="pakfile name: "
echo Files in source:
DIR /S /B %SourceDir%
..\..\_7z\7za.exe a -tzip -mx0 -xr!*.md -xr!.git* "%workshopBuildDir%\%fName%.pak" "%SourceDir%\*"
pause