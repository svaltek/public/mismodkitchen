@echo off
set thisPATH=%~dp0


:Config
REM -- Change this if you keep your pak files somewhere else
set workshopBuildDir=WORKSHOP_BUILD
REM -- Where should we find the mod.vdf -- Defaults to the same folder
set targetFolder=%thisPATH%





:START
REM -- Dont Edit Below this Line
TITLE MisModKitchen: Checking...
echo Current Path: %thisPATH%
set targetFile=%targetFolder%mod.vdf
set contentFolder=%targetFolder%%workshopBuildDir%
set contentFolder=%contentFolder:\=\\%
echo Checking VDF
echo .
IF NOT EXIST "%thisPATH%mod.vdf" ( CALL :CREATE_VDF )





:STEAMCMD 
TITLE MisModKitchen: Publishing...
set /p id="Enter Steam Login: "
..\..\_SteamCMD\steamcmd.exe +login %id% +workshop_build_item %targetFile% +quit
pause





:QUIT
exit



:CREATE_VDF
TITLE MisModKitchen: Creating VDF
echo mod.vdf NOT FOUND... Creating...
echo --------------------------------------------------------------------------------------
echo    Target: %targetFile%
echo    ContentFolder: %contentfolder%
echo . We are now going to create the mods VDF File..
echo .
echo . - Lets Give it a Name....
echo .
echo .
echo --------------------------------------------------------------------------------------
set /p modTitle="Mod Title: "
echo .
echo .
cls
echo Provide a short Description...
echo ------------------------------
echo IMPORTANT: This Should NOT contain special characters, keep things short and simple
echo Then edit the Workshop Page to add  more info..... 
echo .
echo --------------------------------------------------------------------------------------
set /p modInfo="Enter a Description: "
echo .
echo lets wrap things up with a simple changenote....
echo ------------------------------------------------
set /p modNote="Changenote: "

echo "workshopitem" > %targetFile%
echo { >> %targetFile%
echo 	"appid"		"299740" >> %targetFile%
echo 	"contentfolder"		"%contentFolder%" >> %targetFile%
echo 	"visibility"		"0" >> %targetFile%
echo 	"title"		"%modTitle%" >> %targetFile%
echo 	"description"		"%modInfo%" >> %targetFile%
echo 	"changenote"		"%modNote%" >> %targetFile%
echo 	"publishedfileid"		"" >> %targetFile%
echo } >> %targetFile%
GOTO :EOF